## After Generation of Angular Fullstack Application
* add `Gruntfile.js` to `jshint:all` array in `Gruntfile.js` (and fix errors)
* add `*.png binary` to `.gitattributes`
* run `dos2unix` on files with Windows line endings
* add `process.env.PROD_MONGODB` to `mongo.uri` in `server/config/environment/production.js`
* once deployed to Heroku, set the `PROD_MONGODB` environment variable:
  ```
  heroku config:set PROD_MONGODB=mongodb://dbuser:dbpass@host:port/dbname
  ```
* add `SESSION_SECRET` to `server/config/environment/index.js`:
  ```
  secrets: {
    session: process.env.SESSION_SECRET || 'voting-secret'
  },
  ```
* on Heroku, set SESSION_SECRET:
  ```
  heroku config:set SESSION_SECRET=<secret>
  ```
* set Twitter auth variables for Heroku:
  ```
  heroku config:set TWITTER_ID=<id>
  heroku config:set TWITTER_SECRET=<secret>
  heroku config:set DOMAIN=<app-name>.herokuapp.com
  ```